# OwlRWD
New experience of writing RWD web sites with OwlRWD.<br>
Status: Released.<br>
<hr>
## How to use OwlRWD CSS framework:
<ol>
<li>Download latest version</li>
<li>Write this lines in your web page html file:<br>
<code>
&lt;meta name="viewport" content="width=device-width, initial-scale=1"&gt;
</code>For Normal version:<br><code>
&lt;link rel="stylesheet" href="css/owl_rwd.css"&gt;<br>
</code>For MIN version:<br><code>
&lt;link rel="stylesheet" href="css/owl_rwd.min.css"&gt;<br>
</code>For Grid version:<br><code>
&lt;link rel="stylesheet" href="css/owl_rwd.grid.css"&gt;<br>
</code>For Grid MIN version:<br><code>
&lt;link rel="stylesheet" href="css/owl_rwd.grid.min.css"&gt;<br>
</code></li>
<li>Use classes in your html tags</li>
</ol><br>
<hr>
## How to use OwlRWD JS.SCRIPT:
<ol>
<li>Download latest version</li>
<li>Write this lines in your web page html file:<br>
<code>
&lt;script type="text/javascript" src="js/owl_rwd.script.js"&gt;&lt;/script&gt;
</code> and/or <code>&lt;script type="text/javascript" src="js/owl_rwd.script.js"&gt;&lt;/script&gt;</code>
</li>
<li>Use OwlRWD JS.SCRIPT functions in your scripts and web sites</li>
</ol>
<hr>
## How to use OwlRWD CSS.TAG:
<ol>
<li>Download latest version</li>
<li>Write this lines in your web page html file:<br>
<code>
&lt;link rel="stylesheet" href="css/owl_rwd.tag.css"&gt;
</code> for normal version<br>or<br>
<code>&lt;link rel="stylesheet" href="css/owl_rwd.tag.pro.css"&gt;</code> for pro version
</li>
<li>Use normal (custom stylized by CSS.TAG) html tags without OwlRWD CSS classes</li>
</ol><br>
<hr>
## How to use OwlRWD JS.TAG:
<ol>
<li>Download latest version</li>
<li>Write this lines in your web page html file:<br>
<code>
&lt;script type="text/javascript" src="js/owl_rwd.tag.js"&gt;&lt;/script&gt;
</code>
</li>
<li>Use OwlRWD JS.TAG custom tags in your html files</li>
</ol><br>
<hr>
## How to use OwlRWD addons
<ol>
<li>Download latest version</li>
<li>Write this lines in your web page html file:<br>
For modernStyle addon:<br>
<code>
&lt;script type="text/javascript" src="addons/owl_modernStyle.css"&gt;&lt;/script&gt;
</code>
For animatePack[InDev] addon:<br>
<code>
&lt;script type="text/javascript" src="addons/owl_animatePack[InDev].css"&gt;&lt;/script&gt;
</code>
</li>
<li>Use OwlRWD JS.TAG custom tags in your html files</li>
</ol><br>
<hr>
by Kamil Dudar 2017 (this repo is new version of old repo)
