###OwlRWD JS.SCRIPT 1.0 by Kamil Dudar###

###CSS manipulation version###

###Github repository: http://www.owlr.github.io/owl-rwd###

###GitLab repository: https://gitlab.com/Owlr/owl-rwd###

# UNIVERSAL STYLING
# by id

styleElementId = (idName, prop, val) ->
  document.getElementById(idName).style.setProperty prop, val
  return

# by class

styleElementClass = (className, prop, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.setProperty prop, val
  return

# DISPLAY
# Hiding HTML element by id

hideId = (idName) ->
  document.getElementById(idName).style.display = 'none'
  return

# Hiding HTML element by class

hideClass = (className) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.display = 'none'
  return

# Showing HTML element by id

showId = (idName) ->
  document.getElementById(idName).style.display = 'block'
  return

# Showing HTML element by class

showClass = (className) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.display = 'block'
  return

# Universal display property manipulation by id

displayId = (idName, property) ->
  document.getElementById(idName).style.display = property
  return

# Universal display property manipulation by class

displayClass = (className, property) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.display = property
  return

# FONT SIZE
# Universal font size property manipulation by id

fontSizeId = (idName, fontSize) ->
  document.getElementById(idName).style.fontSize = fontSize
  return

# Universal font size property manipulation by class

fontSizeClass = (className, fontSize) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.fontSize = fontSize
  return

# FONT COLOR
# Universal font color property manipulation by id

textColorId = (idName, fontColor) ->
  document.getElementById(idName).style.color = fontColor
  return

# Universal font color property manipulation by class

textColorClass = (className, fontColor) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.color = fontColor
  return

# OPACITY
# Universal opacity property manipulation by id

opacityId = (idName, opacity) ->
  document.getElementById(idName).style.opacity = opacity
  return

# Universal opacity property manipulation by class

opacityClass = (className, opacity) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.opacity = opacity
  return

# POSITION
# Universal position property manipulation by id

positionId = (idName, position) ->
  document.getElementById(idName).style.position = position
  return

# Universal position property manipulation by class

positionClass = (className, position) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.position = position
  return

# Universal position (side) property manipulation by id

elemPositionId = (idName, side, val) ->
  doc = document.getElementById(idName)
  switch side
    when top
      doc.style.top = val
    when bottom
      doc.style.bottom = val
    when left
      doc.style.left = val
    when right
      doc.style.right = val
  return

# Universal position (side) property manipulation by class

elemPositionClass = (className, side, val) ->
  doc = document.getElementsByClassName(className)
  switch side
    when top
      doc[0].style.top = val
    when bottom
      doc[0].style.bottom = val
    when left
      doc[0].style.left = val
    when right
      doc[0].style.right = val
  return

# WIDTH AND HEIGHT
# Universal width property manipulation by id

widthId = (idName, val) ->
  document.getElementById(idName).style.width = val
  return

# Universal width property manipulation by class

widthClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.width = val
  return

# Universal height property manipulation by id

heightId = (idName, val) ->
  document.getElementById(idName).style.height = val
  return

# Universal height property manipulation by class

heightClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.height = val
  return

# FLOAT AND CLEAR
# Universal float property manipulation by id

floatId = (idName, pos) ->
  document.getElementById(idName).style.float = pos
  return

# Universal float property manipulation by class

floatClass = (className, pos) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.float = pos
  return

# Universal clear property manipulation by id

clearId = (idName, clear) ->
  document.getElementById(idName).style.clear = clear
  return

# Universal clear property manipulation by class

clearClass = (className, clear) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.clear = clear
  return

# PADDING
# Universal padding property manipulation by id

paddingId = (idName, padding, val) ->
  doc = document.getElementById(idName)
  switch padding
    when 'all'
      doc.style.padding = val
    when 'top'
      doc.style.paddingTop = val
    when 'bottom'
      doc.style.paddingBottom = val
    when 'left'
      doc.style.paddingLeft = val
    when 'right'
      doc.style.paddingRight = val
  return

# Universal padding property manipulation by class

paddingClass = (className, padding, val) ->
  doc = document.getElementsByClassName(className)
  switch padding
    when 'all'
      doc[0].style.padding = val
    when 'top'
      doc[0].style.paddingTop = val
    when 'bottom'
      doc[0].style.paddingBottom = val
    when 'left'
      doc[0].style.paddingLeft = val
    when 'right'
      doc[0].style.paddingRight = val
  return

# MARGIN
# Universal margin property manipulation by id

marginId = (idName, margin, val) ->
  doc = document.getElementById(idName)
  switch margin
    when 'all'
      doc.style.margin = val
    when 'top'
      doc.style.marginTop = val
    when 'bottom'
      doc.style.marginBottom = val
    when 'left'
      doc.style.marginLeft = val
    when 'right'
      ddoc.style.marginRight = val
  return

# Universal margin property manipulation by class

marginClass = (className, margin, val) ->
  doc = document.getElementsByClassName(className)
  switch margin
    when 'all'
      doc[0].style.margin = val
    when 'top'
      doc[0].style.marginTop = val
    when 'bottom'
      doc[0].style.marginBottom = val
    when 'left'
      doc[0].style.marginLeft = val
    when 'right'
      doc[0].style.marginRight = val
  return

# BORDER
# Universal border property manipulation by id

borderId = (idName, border, val) ->
  doc = document.getElementById(idName)
  switch border
    when 'all'
      doc.style.border = val
    when 'top'
      doc.document.getElementById(idName).style.borderTop = val
    when 'bottom'
      doc.document.getElementById(idName).style.borderBottom = val
    when 'left'
      doc.document.getElementById(idName).style.borderLeft = val
    when 'right'
      doc.document.getElementById(idName).style.borderRight = val
  return

# Universal border property manipulation by class

borderClass = (className, border, val) ->
  doc = document.getElementsByClassName(className)
  switch border
    when 'all'
      doc[0].style.border = val
    when 'top'
      doc[0].style.borderTop = val
    when 'bottom'
      doc[0].style.borderBottom = val
    when 'left'
      doc[0].style.borderLeft = val
    when 'right'
      doc[0].style.borderRight = val
  return

# BORDER RADIUS
# Universal border radius property manipulation by id

borderRadiusId = (idName, val) ->
  document.getElementById(idName).style.broderRadius = val
  return

# Universal border radius property manipulation by class

borderRadiusClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.borderRadius = val
  return

# BORDER COLOR
# Universal border color property manipulation by id

borderColorId = (idName, val) ->
  document.getElementById(idName).style.broderColor = val
  return

# Universal border color property manipulation by class

borderColorClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.borderColor = val
  return

# TEXT ALIGN
# Universal text align property manipulation by id

textAlignId = (idName, val) ->
  document.getElementById(idName).style.textAlign = val
  return

# Universal text align property manipulation by class

textAlignClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.textAlign = val
  return

# LETTER SPACING
# Universal letter spacing property manipulation by id

letterSpacingId = (idName, val) ->
  document.getElementById(idName).style.letterSpacing = val
  return

# Universal letter spacing property manipulation by class

letterSpacingClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.letterSpacing = val
  return

# FONT FAMILY
# Universal font family property manipulation by id

fontFamilyId = (idName, val) ->
  document.getElementById(idName).style.fontFamily = val
  return

# Universal font family property manipulation by class

fontFamilyClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.fontFamily = val
  return

# FONT STRETCH
# Universal font stretch property manipulation by id

fontStretchId = (idName, val) ->
  document.getElementById(idName).style.fontStretch = val
  return

# Universal font stretch property manipulation by class

fontStretchClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.fontStretch = val
  return

# WORD WRAP
# Universal word wrap property manipulation by id

wordWrapId = (idName, val) ->
  document.getElementById(idName).style.wordWrap = val
  return

# Universal word wrap property manipulation by class

wordWrapClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.wordWrap = val
  return

# LINE HEIGHT
# Universal line height property manipulation by id

lineHeightId = (idName, val) ->
  document.getElementById(idName).style.lineHeight = val
  return

# Universal line height property manipulation by class

lineHeightClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.lineHeight = val
  return

# BACKGROUND COLOR
# Universal background color property manipulation by id

backgroundColorId = (idName, val) ->
  document.getElementById(idName).style.backgroundColor = val
  return

# Universal background color property manipulation by class

backgroundColorClass = (className, val) ->
  doc = document.getElementsByClassName(className)
  doc[0].style.backgroundColor = val
  return

# ---
# generated by js2coffee 2.2.0
