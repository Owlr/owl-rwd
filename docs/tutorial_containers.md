# OwlRWD v1.2 Framework Tutorial
<hr>
## Containers
<div style="padding: 0.01em 15px;"><b>.owl-container</b> - top and bottom have 0.01em paddings, left and right have 15px paddings.</div><br>
<div style="padding: 0.01em 15px; margin-top: 15px; margin-bottom: 15px;"><b>.owl-panel</b> - paddings same as at .owl-container, top and bottom have 15px margins</div><br>
<div style="box-shadow: 0 2px 5px 0 #aaa, 0 2px 10px 0 #ccc;"><b>.owl-card</b> or <b>.owl-card-2</b> - have small shadow</div><br>
<div style="box-shadow: 0 4px 5px 0 #aaa, 0 2px 10px 0 #ccc;"><b>.owl-card-4</b> or <b>.owl-hover-shadow</b> - have medium shadow</div><br>
<div style="box-shadow: 0 6px 5px 0 #aaa, 0 2px 10px 0 #ccc;"><b>.owl-card-6</b> - have large shadow</div><br>
<div style="box-shadow: 0 8px 5px 0 #aaa, 0 2px 10px 0 #ccc;"><b>.owl-card-8</b> - have extra large shadow</div>
