# OwlRWD v1.2 Framework Tutorial
<hr>
## Responsive grid
|| 1st column | 2nd column | 3rd column | 4th column | 5th column | 6th column | 7th column | 8th column | 9th column | 10th column | 11th column | 12th column |
|:--|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:--|
| .owl-col- | xs1 | xs2 | xs3 | xs4 | xs5 | xs6 | xs7 | xs8 | xs9 | xs10 | xs11 | xs12 | min-width: 390px |
| .owl-col- | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | s10 | s11 | s12 | min-width: 520px |
| .owl-col- | m1 | m2 | m3 | m4 | m5 | m6 | m7 | m8 | m9 | m10 | m11 | m12 | min-width: 728px |
| .owl-col- | l1 | l2 | l3 | l4 | l5 | l6 | l7 | l8 | l9 | l10 | l11 | l12 | min-width: 991px |
| .owl-col- | xl1 | xl2 | xl3 | xl4 | xl5 | xl6 | xl7 | xl8 | xl9 | xl10 | xl11 | xl12 | min-width: 1280px |
| .owl-col- | n1 | n2 | n3 | n4 | n5 | n6 | n7 | n8 | n9 | n10 | n11 | n12 | <del>min-width</del> |
| .owl-col-alt- | s1 | s2 | s3 | s4 | s5 | s6 | s7 | s8 | s9 | s10 | min-width: 520px |
| .owl-col-alt- | m1 | m2 | m3 | m4 | m5 | m6 | m7 | m8 | m9 | m10 | min-width: 728px |
| .owl-col-alt- | l1 | l2 | l3 | l4 | l5 | l6 | l7 | l8 | l9 | l10 | min-width: 991px; |
| .owl-col-alt- | n1 | n2 | n3 | n4 | n5 | n6 | n7 | n8 | n9 | n10 | <del>min-width</del> |
## Warning:
###### .owl-col family have 12 columns (8.33%, 16.66%, 25%, 33.33%, 41.66%, 50%, 58.33%, 66.66%, 75%, 83.33%, 91.66%, 100%)
###### .owl-col-alt family have 10 columns (10%, 20%, 30%, 40%, 50%, 60%, 70%, 80%, 90%, 100%)
