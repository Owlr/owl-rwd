/*OwlRWD JS.SCRIPT 1.0 by Kamil Dudar*/
/*All no-CSS manip scripts*/
/*Github repository: http://www.owlr.github.io/owl-rwd*/
/*GitLab repository: https://gitlab.com/Owlr/owl-rwd*/

// INNERHTML
function addHtmlId(idName, add) {
  document.getElementById(idName).innerHTML = add;
}
function addHtmlClass(className, add) {
  var doc = document.getElementsByClassName(className);
  doc[0].innerHTML = add;
}

// LINKS (MAY NOT WORK, BECAUSE THIS IS NOT IN JS STANDARD)
function link(idName, cont, link) {
  var str = cont;
  var result = str.link(link);
  document.getElementById(idName).innerHTML = result;
}

// FORMS
// Form validation (without 'required' HTML attribute) <1>
function validFormMsg(fieldName, msgName) {
    var doc = document.getElementById(fieldName);
    if (doc.checkValidity() == false) {
        document.getElementById(msgName).innerHTML = doc.validationMessage;
    }
}
// Form validation (with or without 'required' HTML attribute) <2>
function validFormMsgAll(fieldName, msgName) {
    var doc = document.getElementById(fieldName);
    var msg = document.getElementById(msgName);
    if (doc.validity.rangeOverflow) {
      msg.innerHTML = fieldName + "'s value is too large";
    } else if (doc.validity.rangeUnderflow) {
      msg.innerHTML = fieldName + "'s value is too small"
    } else if (doc.validity.tooLong) {
      msg.innerHTML = fieldName + "'s value is too long"
    } else if (doc.validity.valueMissing) {
      msg.innerHTML = fieldName + "'s value is missing"
    }
}
// Form validation (without 'required' HTML attribute) <3>
function validFormAlert(fontName, fieldName) {
    var doc = document.forms[formName][fieldName].value;
    if (doc == "") {
        alert(fieldName + " must be filled out");
        return false;
    }
}

// OTHER SCRIPTS
function goBack() {
  window.history.back();
}

function goForward() {
  window.history.forward();
}
