/*OwlRWD JS.SCRIPT 1.0 by Kamil Dudar*/
/*CSS manipulation version*/
/*Github repository: http://www.owlr.github.io/owl-rwd*/
/*GitLab repository: https://gitlab.com/Owlr/owl-rwd*/

// UNIVERSAL STYLING
// by id
function styleElementId(idName, prop, val) {
  document.getElementById(idName).style.setProperty(prop, val);
}
// by class
function styleElementClass(className, prop, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.setProperty(prop, val);
}

// DISPLAY
// Hiding HTML element by id
function hideId(idName) {
  document.getElementById(idName).style.display = "none";
}
// Hiding HTML element by class
function hideClass(className) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.display = "none";
}
// Showing HTML element by id
function showId(idName) {
  document.getElementById(idName).style.display = "block";
}
// Showing HTML element by class
function showClass(className) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.display = "block";
}
// Universal display property manipulation by id
function displayId(idName, property) {
  document.getElementById(idName).style.display = property;
}
// Universal display property manipulation by class
function displayClass(className, property) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.display = property;
}

// FONT SIZE
// Universal font size property manipulation by id
function fontSizeId(idName, fontSize) {
  document.getElementById(idName).style.fontSize = fontSize;
}
// Universal font size property manipulation by class
function fontSizeClass(className, fontSize) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.fontSize = fontSize;
}

// FONT COLOR
// Universal font color property manipulation by id
function textColorId(idName, fontColor) {
  document.getElementById(idName).style.color = fontColor;
}
// Universal font color property manipulation by class
function textColorClass(className, fontColor) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.color = fontColor;
}

// OPACITY
// Universal opacity property manipulation by id
function opacityId(idName, opacity) {
  document.getElementById(idName).style.opacity = opacity;
}
// Universal opacity property manipulation by class
function opacityClass(className, opacity) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.opacity = opacity;
}

// POSITION
// Universal position property manipulation by id
function positionId(idName, position) {
  document.getElementById(idName).style.position = position;
}
// Universal position property manipulation by class
function positionClass(className, position) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.position = position;
}
// Universal position (side) property manipulation by id
function elemPositionId(idName, side, val) {
  var doc = document.getElementById(idName);
  switch (side) {
    case top:
      doc.style.top = val;
    break;
    case bottom:
      doc.style.bottom = val;
    break;
    case left:
      doc.style.left = val;
    break;
    case right:
      doc.style.right = val;
    break;
  }
}
// Universal position (side) property manipulation by class
function elemPositionClass(className, side, val) {
  var doc = document.getElementsByClassName(className);
  switch (side) {
    case top:
      doc[0].style.top = val;
    break;
    case bottom:
      doc[0].style.bottom = val;
    break;
    case left:
      doc[0].style.left = val;
    break;
    case right:
      doc[0].style.right = val;
    break;
  }
}

// WIDTH AND HEIGHT
// Universal width property manipulation by id
function widthId(idName, val) {
  document.getElementById(idName).style.width = val;
}
// Universal width property manipulation by class
function widthClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.width = val;
}
// Universal height property manipulation by id
function heightId(idName, val) {
  document.getElementById(idName).style.height = val;
}
// Universal height property manipulation by class
function heightClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.height = val;
}

// FLOAT AND CLEAR
// Universal float property manipulation by id
function floatId(idName, pos) {
  document.getElementById(idName).style.float = pos;
}
// Universal float property manipulation by class
function floatClass(className, pos) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.float = pos;
}
// Universal clear property manipulation by id
function clearId(idName, clear) {
  document.getElementById(idName).style.clear = clear;
}
// Universal clear property manipulation by class
function clearClass(className, clear) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.clear = clear;
}

// PADDING
// Universal padding property manipulation by id
function paddingId(idName, padding, val) {
  var doc = document.getElementById(idName);
  switch (padding) {
    case "all":
        doc.style.padding = val;
    break;
    case "top":
        doc.style.paddingTop = val;
    break;
    case "bottom":
        doc.style.paddingBottom = val;
    break;
    case "left":
        doc.style.paddingLeft = val;
    break;
    case "right":
        doc.style.paddingRight = val;
    break;
  }
}
// Universal padding property manipulation by class
function paddingClass(className, padding, val) {
  var doc = document.getElementsByClassName(className);
  switch (padding) {
    case "all":
        doc[0].style.padding = val;
    break;
    case "top":
        doc[0].style.paddingTop = val;
    break;
    case "bottom":
        doc[0].style.paddingBottom = val;
    break;
    case "left":
        doc[0].style.paddingLeft = val;
    break;
    case "right":
        doc[0].style.paddingRight = val;
    break;
  }
}

// MARGIN
// Universal margin property manipulation by id
function marginId(idName, margin, val) {
  var doc = document.getElementById(idName);
  switch (margin) {
    case "all":
        doc.style.margin = val;
    break;
    case "top":
        doc.style.marginTop = val;
    break;
    case "bottom":
        doc.style.marginBottom = val;
    break;
    case "left":
        doc.style.marginLeft = val;
    break;
    case "right":
        ddoc.style.marginRight = val;
    break;
  }
}
// Universal margin property manipulation by class
function marginClass(className, margin, val) {
  var doc = document.getElementsByClassName(className);
  switch (margin) {
    case "all":
        doc[0].style.margin = val;
    break;
    case "top":
        doc[0].style.marginTop = val;
    break;
    case "bottom":
        doc[0].style.marginBottom = val;
    break;
    case "left":
        doc[0].style.marginLeft = val;
    break;
    case "right":
        doc[0].style.marginRight = val;
    break;
  }
}

// BORDER
// Universal border property manipulation by id
function borderId(idName, border, val) {
  var doc = document.getElementById(idName);
  switch (border) {
    case "all":
        doc.style.border = val;
    break;
    case "top":
        doc.document.getElementById(idName).style.borderTop = val;
    break;
    case "bottom":
        doc.document.getElementById(idName).style.borderBottom = val;
    break;
    case "left":
        doc.document.getElementById(idName).style.borderLeft = val;
    break;
    case "right":
        doc.document.getElementById(idName).style.borderRight = val;
    break;
  }
}
// Universal border property manipulation by class
function borderClass(className, border, val) {
  var doc = document.getElementsByClassName(className);
  switch (border) {
    case "all":
        doc[0].style.border = val;
    break;
    case "top":
        doc[0].style.borderTop = val;
    break;
    case "bottom":
        doc[0].style.borderBottom = val;
    break;
    case "left":
        doc[0].style.borderLeft = val;
    break;
    case "right":
        doc[0].style.borderRight = val;
    break;
  }
}

// BORDER RADIUS
// Universal border radius property manipulation by id
function borderRadiusId(idName, val) {
  document.getElementById(idName).style.broderRadius = val;
}
// Universal border radius property manipulation by class
function borderRadiusClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.borderRadius = val;
}

// BORDER COLOR
// Universal border color property manipulation by id
function borderColorId(idName, val) {
  document.getElementById(idName).style.broderColor = val;
}
// Universal border color property manipulation by class
function borderColorClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.borderColor = val;
}

// TEXT ALIGN
// Universal text align property manipulation by id
function textAlignId(idName, val) {
  document.getElementById(idName).style.textAlign = val;
}
// Universal text align property manipulation by class
function textAlignClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.textAlign = val;
}

// LETTER SPACING
// Universal letter spacing property manipulation by id
function letterSpacingId(idName, val) {
  document.getElementById(idName).style.letterSpacing = val;
}
// Universal letter spacing property manipulation by class
function letterSpacingClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.letterSpacing = val;
}

// FONT FAMILY
// Universal font family property manipulation by id
function fontFamilyId(idName, val) {
  document.getElementById(idName).style.fontFamily = val;
}
// Universal font family property manipulation by class
function fontFamilyClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.fontFamily = val;
}

// FONT STRETCH
// Universal font stretch property manipulation by id
function fontStretchId(idName, val) {
  document.getElementById(idName).style.fontStretch = val;
}
// Universal font stretch property manipulation by class
function fontStretchClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.fontStretch = val;
}

// WORD WRAP
// Universal word wrap property manipulation by id
function wordWrapId(idName, val) {
  document.getElementById(idName).style.wordWrap = val;
}
// Universal word wrap property manipulation by class
function wordWrapClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.wordWrap = val;
}

// LINE HEIGHT
// Universal line height property manipulation by id
function lineHeightId(idName, val) {
  document.getElementById(idName).style.lineHeight = val;
}
// Universal line height property manipulation by class
function lineHeightClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.lineHeight = val;
}

// BACKGROUND COLOR
// Universal background color property manipulation by id
function backgroundColorId(idName, val) {
  document.getElementById(idName).style.backgroundColor = val;
}
// Universal background color property manipulation by class
function backgroundColorClass(className, val) {
  var doc = document.getElementsByClassName(className);
  doc[0].style.backgroundColor = val;
}
