/*OwlRWD JS.TAG 1.0.2 CEv0 by Kamil Dudar*/
/*OwlRWD HTML Custom Tags*/
/*This version uses Custom Elements v0*/
/*Github repository: http://www.owlr.github.io/owl-rwd*/
/*GitLab repository: https://gitlab.com/Owlr/owl-rwd*/

var CRed = document.registerElement('code-red'); document.body.appendChild(new CRed()); //Code red
var CBlue = document.registerElement('code-blue'); document.body.appendChild(new CBlue()); //Code blue
var CGreen = document.registerElement('code-green'); document.body.appendChild(new CGreen()); //Code green
var AdressSerif = document.registerElement('adress-serif'); document.body.appendChild(new AdressSerif()); //Serif-style adress
var BigButton = document.registerElement('button-big'); document.body.appendChild(new BigButton()); //Big button
var Animate = document.registerElement('animate-block'); document.body.appendChild(new Animate()); //Animate
var AnimateTop = document.registerElement('animate-top'); document.body.appendChild(new AnimateTop()); //Animate to top
var AnimateBottom = document.registerElement('animate-bottom'); document.body.appendChild(new AnimateBottom()); //Aniamte to bottom
var AnimateLeft = document.registerElement('animate-left'); document.body.appendChild(new AnimateLeft()); //Animate to left
var AnimateRight = document.registerElement('animate-right'); document.body.appendChild(new AnimateRight()); //Animate to right
var AnimateOp = document.registerElement('animate-opacity'); document.body.appendChild(new AnimateOp()); //Opacity animation
var AnimateZoom = document.registerElement('animate-zoom'); document.body.appendChild(new AnimateZoom()); //Zoom animation
var AnimateFade = document.registerElement('animate-fading'); document.body.appendChild(new AnimateFade()); //Fade animation
var AnimateSpin = document.registerElement('animate-spin'); document.body.appendChild(new AnimateSpin()); //Spin animation
var DispNone = document.registerElement('display-none'); document.body.appendChild(new DispNone()); //Display none
var owlH7 = document.registerElement('o-h7'); document.body.appendChild(new owlH7()); //Small H7 header
var owlH0 = document.registerElement('o-h0'); document.body.appendChild(new owlH0()); //Jumbo H0 header
var textSerif = document.registerElement('text-serif'); document.body.appendChild(new textSerif()); //Serif-style text
var textSansSerif = document.registerElement('text-sans-serif'); document.body.appendChild(new textSansSerif()); //Sans serif-style text
var textMono = document.registerElement('text-monospace'); document.body.appendChild(new textMono()); //Monospace-style text
var textCurs = document.registerElement('text-cursive'); document.body.appendChild(new textCurs()); //Cursive-style text
var textFant = document.registerElement('text-fantasy'); document.body.appendChild(new textFant()); //Fantasy-style text
